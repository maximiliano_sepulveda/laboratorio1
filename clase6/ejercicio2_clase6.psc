Proceso ejercicio2_clase6
	definir a,b,c Como Entero;
	a<-0;
	b<-0;
	c<-0;
///forzamos que los tres valores sean diferentes
	mientras a=b o b=c o a=c Hacer
		escribir "ingresar 3 valores";
		leer a,b,c;
		///para alertar que hay numeros iguales utilizamos un si 
		///con la miasma condicion del mientras
		si a=b o b=c o a=c Entonces
			escribir "hay valores iguales. ingrese nuevamente";
		FinSi
	FinMientras
	///muestra cual es el numero mayor de los 3 ingresados
	si a>b y a>c Entonces
		escribir a," es mayor";
	SiNo
		si b>a y b>c Entonces
			escribir b," es mayor";
		
		sino
			escribir c," es mayor";
		FinSi
	FinSi
	///muestra cual es el valor menor de los 3 ingresados
	si a<b y a<c Entonces
		escribir a," es menor";
	SiNo
		si b<a y b<c Entonces
			escribir b," es menor";
			
		sino
			escribir c," es menor";
		FinSi
	FinSi
FinProceso
